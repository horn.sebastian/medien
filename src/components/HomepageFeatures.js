import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'What',
    img: '/img/network.png',
    description: (
      <>
        The LINCS infrastructure project will convert large datasets into an organized,
        interconnected, machine-processable set of resources for Canadian cultural research.
      </>
    ),
  },
  {
    title: 'Why',
    img: '/img/data-collection.png',
    description: (
      <>
        LINCS aims to provide context for the cultural material that currently floats around online,
        interlink it, ground it in its sources,
        and help to make the World Wide Web a trusted resource for scholarly knowledge production.
      </>
    ),
  },
  {
    title: 'Who',
    img: '/img/globe.png',
    description: (
      <>
        With a team of technical and domain experts, LINCS will allow Canadian scholars and
        partner institutions to play a significant role in the developing the Semantic Web.
      </>
    ),
  },
];

function Feature({img, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <img src={img} className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
