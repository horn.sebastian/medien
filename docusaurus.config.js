// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Medien',
  tagline: 'Dokus für das Medienteam',
  url: 'https://pages.gitlab.io',
  baseUrl: '/medien/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'horn.sebastian', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/horn.sebastian/medien/tree/master/'//https://gitlab.com/calincs/admin/docusaurus/-/tree/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      hideableSidebar: true,
      autoCollapseSidebarCategories: true,
      navbar: {
        title: 'LINCSaurus',
        logo: {
          alt: 'LINCS Logo',
          src: 'img/lincs_logo.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'tutorial/intro',
            position: 'left',
            label: 'Docusaurus Tutorial',
          },
          {
            type: 'doc',
            docId: 'nerve/nerve-test-documentation',
            position: 'left',
            label: 'NERVE',
          },
          {
            href: 'https://gitlab.com/horn.sebastian/medien', //'https://gitlab.com/calincs/admin/docusaurus',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Learn',
            items: [
              {
                label: 'LINCS project website',
                to: 'https://lincsproject.ca/',
              },
              {
                label: 'Docusaurus Tutorial',
                to: '/docs/tutorial/intro',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'Email us',
                href: 'mailto://lincs.project@gmail.com',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/lincsproject',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Blog',
                to: 'https://lincsproject.ca/blog/',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/calincs/admin/docusaurus',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Sebastian Horn.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
