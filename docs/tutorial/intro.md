# Docusaurus Introduction

This tutorial just covers the basics of editing docs in Docusaurus. Refer to the [main Docusaurus site](https://docusaurus.io/docs/docs-introduction) for detailed guides.

## Installation

To work effectively on the LINCS docusaurus site, you'll need Git (with an ssh key set up to connect to GitLab), NodeJS (v16 or newer), and VSCode. After installing and configuring all of these, you'll need to create a [GitLab SSH key](https://docs.gitlab.com/ee/ssh/) to work with the repository. Once that is set up, go ahead and clone the repo:

```bash
git clone git@gitlab.com:calincs/admin/docusaurus.git
cd docusaurus
```

## Local Development

```bash
npm install
npm start
```

This command starts a local development server at `http://localhost:3000/` and opens up a browser window. Most changes are reflected live without having to restart the server.

## Deployment

Commit and push the changes back to the upstream repo on GitLab.
