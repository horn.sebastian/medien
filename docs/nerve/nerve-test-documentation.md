---
sidebar_position: 1
---

# NERVE Documentation - test

## Entity annotations

### Brief description

Entity annotations are chunks of computer code applied to pieces of text to label them as entities. NERVE can propose named entity candidates; you can use CWRC-Writer to approve these entity candidates, create entity annotations (i.e., automatically generate annotations), and apply annotations to the entities.

### Body

#### Entity annotations using CWRC-Writer

Using CWRC-Writer, you can create and apply entity annotations to named entity candidates identified using NERVE. These entity annotations can function within and beyond the document in which they are created. CWRC-Writer can also be used to create other entities (e.g., links or keywords) outside of NERVE.

CWRC-Writer can create two types of entity annotations: RDF (Linked Open Data) and XML code. It can also create both types of entity annotations at the same time.

<table>
  <tr>
   <td>Entity type
   </td>
   <td>Category
   </td>
   <td>How is the entity stored?
   </td>
  </tr>
  <tr>
   <td>RDF (Linked Open Data)
   </td>
   <td>Standoff
   </td>
   <td>Entity is stored in the file’s metadata section
   </td>
  </tr>
  <tr>
   <td>XML
   </td>
   <td>In situ
   </td>
   <td>Entity is stored at the entry location
   </td>
  </tr>
</table>

CWRC-Writer uses the World Wide Web Consortium's [Web Annotation](https://www.w3.org/TR/annotation-model/) standard for annotating documents.

#### Entity reconciliation (entity linking)

Entity reconciliation, or entity linking, is part of the entity annotation process. In entity reconciliation, you link an entity candidate with a URI (uniform resource identifier) that connects to an authoritative entity record.

A URI is a unique address for a resource. URIs can point to entity records that are proprietary to a specific project, or they can point to entity records produced, maintained, and stored by authorities on the Web. Examples of authorities that manage entity records include [VIAF](http://viaf.org/), [Wikidata](https://www.wikidata.org), and [GeoNames](https://www.geonames.org/). NERVE provides a list of authorities for you to use.

When an entity candidate is reconciled, the details for a URI are added to the entity’s text encoding: either XML (in situ), RDF (JSON-LD/standoff), or both. By connecting the entity candidate to a URI, the entity candidate is resolved (i.e., it loses its candidate status to become an entity in its own right; it therefore no longer appears in the NERVE panel, which only lists named entity candidates). An entity is unambiguously defined by its URI. In the case of NERVE and named entities, this means that the entity is definitively established to be the place, person, organization, work, or other thing specified by the URI.

Reconciled entities follow standard Web protocols, which means that the information they contain can be understood across (and in relation to other information that is part of) the Semantic Web. When CWRC-Writer incorporates a URI into the document’s XML or RDF, the entity becomes linked to other material on the Web that also refers to the same entity. This means that, via the URI, your document is connected to other related materials, even if you do not explicitly link to those materials—and even if you have no knowledge of them. 

Using shared, authoritative URIs makes it possible to tell the difference between two entities that could otherwise be confused. To take an example from the [Orlando Project](http://orlando.cambridge.org.subzero.lib.uoguelph.ca/), entity reconciliation enables a computer (and therefore a reader) to determine whether a document that mentions “Mary Cary” is referring to [Mary Cary](http://orlando.cambridge.org/public/svPeople?person_id=carym2) or is actually referring to [Mary Ann Shadd Cary](http://orlando.cambridge.org/public/svPeople?person_id=caryma). Entity reconciliation (i.e., the use of external identifiers or URIs) is a fundamental building block of Linked Open Data.
